﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MediatRNetCoreSample.Commands;

namespace MediatRNetCoreSample.Handlers
{
    public class ActivateAnimalHandler : IRequestHandler<ActivateAnimal, ActivateAnimalResponse>
    {
        public Task<ActivateAnimalResponse> Handle(ActivateAnimal request, CancellationToken cancellationToken)
        {
            return Task.FromResult(new ActivateAnimalResponse { Status = "Activated" });
        }
    }
}
