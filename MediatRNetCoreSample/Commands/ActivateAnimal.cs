﻿using MediatR;

namespace MediatRNetCoreSample.Commands
{
    public class ActivateAnimal : IRequest<ActivateAnimalResponse> { }

    public class ActivateAnimalResponse
    {
        public string Status { get; set; }
    }
}
