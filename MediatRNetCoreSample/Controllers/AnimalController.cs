﻿using System.Threading.Tasks;
using MediatR;
using MediatRNetCoreSample.Commands;
using Microsoft.AspNetCore.Mvc;

namespace MediatRNetCoreSample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AnimalController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("{id}")]
        public async Task<ActivateAnimalResponse> Activate(int id)
        {
            return await _mediator.Send(new ActivateAnimal());
        }
    }
}
